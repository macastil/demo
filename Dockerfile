FROM openjdk:8-jre-alpine
ADD target/*.jar micro.jar
ENTRYPOINT ["java", "-jar", "micro.jar"]
