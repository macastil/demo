pipeline {
    agent {
        label 'slave'
    }

    environment {
        AMBIENTE = setEnv()

        MAVEN_HOME = tool 'm3'
        dockerImage = ''
        DOCKERHUB_CREDENTIALS = credentials('dockerhub')
        REGISTRY='mauron'
        //
        PROJECT_ID = 'arquitectura-digital-everis'
        CLUSTER_NAME = 'k8s-base'
        LOCATION = 'us-west2-a'
        CREDENTIALS_ID = 'gcp'
        BRANCH: ${env.gitlabSourceBranch}
    }

    stages {
        
        stage('Clonar Repositorio') {
            steps {
                script {
         
                  def gitLabRepositoryUrl = 'https://gitlab.com/macastil/demo.git'
    
                  git(
                    url: gitLabRepositoryUrl,
                    branch: $BRANCH,
                    credentialsId: 'gitlab-token'
                  )
                }
            }
        }

        stage('Compilar con Maven') {
            steps {
                script {
                    sh "${MAVEN_HOME}/bin/mvn clean package -DskipTests"
                }
            }
        }

        stage('Análisis SonarQube') {
            steps {
                script {
                    withSonarQubeEnv('sonar') {
                        sh "${MAVEN_HOME}/bin/mvn sonar:sonar"
                    }
                }
            }
        }
        
        stage('Construir y Publicar Imagen Docker') {
            steps {
                script {
                  sh 'docker build . -t $REGISTRY/demo:$BUILD_ID'
                  sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
                  sh 'docker push $REGISTRY/demo:$BUILD_ID'
                 
                }
            }
        }

        stage('Reemplazar valores en el yaml') {
            steps{ 
                script {
                    sh 'sed -i "s/TAG/$BUILD_ID/g" deploy.yml'
                    sh 'sed -i "s/NS/$AMBIENTE/g" deploy.yml'
                }
        }

        
        stage('Deploy to GKE') {
            steps{
                step([
                $class: 'KubernetesEngineBuilder',
                projectId: env.PROJECT_ID,
                clusterName: env.CLUSTER_NAME,
                location: env.LOCATION,
                manifestPattern: 'deploy.yml',
                credentialsId: env.CREDENTIALS_ID,
                verifyDeployments: true])
            }
        }

    }

    post {
        always {
            // Puedes agregar acciones que se ejecuten siempre, como la limpieza de recursos
            cleanWs()
        }
    }

}


def setEnv() {
	if ("${env.gitlabSourceBranch}".contains('develop') ) {
		return "test"
	} else if ("${env.gitlabSourceBranch}".contains('master')){
		return "prd"
	} 
}