package com.everis.paas;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataController {
	
	@Value("${app.url}")
	private String url;

	@GetMapping("/url")
	public String getUrl() {
		return url;
	}
	
	@GetMapping("/")
	public String getLive() {
		return "200";
	}
}
